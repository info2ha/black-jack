import random

class Card:
    '''
    >>> card_1 = Card('Spades', 'Queen')
    >>> card_2 = Card('Hearts', '2')
    >>> card_1.suit
    'Spades'
    >>> card_1.value
    'Queen'
    >>> card_2.suit
    'Hearts'
    >>> card_2.value
    '2'
    >>> repr(card_1)
    'Queen of Spades'
    >>> repr(card_2)
    '2 of Hearts'
    '''

    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

    def __repr__(self):
        return f"{self.value} of {self.suit}"

    def card_value(self):
        '''
        This method returns a value of a single card
        :return: int
        '''
        if self.value in ['Jack', 'Queen', 'King']:
            return 10
        elif self.value == "Ace":
            return 11
        else:
            return int(self.value)


class Deck:

    suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
    values = {2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King', 'Ace'}

    def __init__(self):

        '''
        testing if our cards were initialized correctly

        >>> deck = Deck()
        >>> cards = {f"{value} of {suit}" for suit in Deck.suits for value in Deck.values}
        >>> str_cards = {str(card) for card in cards}
        >>> cards == str_cards
        True
        >>> len(cards)
        52
        '''

        self.cards = []
        for suit in self.suits:
            for value in self.values:
                self.cards.append(Card(suit, value))
        self.shuffle()
    def shuffle(self):

        '''
        shuffle the deck

        >>> deck1 = Deck()
        >>> deck2 = Deck()
        >>> deck1_copy = deck1.cards.copy()
        >>> deck1.shuffle()
        >>> deck1_copy != deck1
        True
        >>> deck2.shuffle()
        >>> deck1 != deck2      # shuffle() gives unique order every time
        True
        '''
        random.shuffle(self.cards)

    def deal(self):
        '''
        return the first card from the deck and delete it from the array

        >>> deck = Deck()
        >>> length = len(deck.cards)
        >>> card = deck.deal()
        >>> new_length = len(deck.cards)
        >>> new_length == length - 1
        True
        >>> isinstance(card, Card)      # the returned object is from Card class
        True
        '''
        return self.cards.pop()




if __name__ == "__main__":
    import doctest
    doctest.testmod()
