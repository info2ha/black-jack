######################################
# Introduction to Python Programming #
# Prof. Dr. Annemarie Friedrich      #
# Dr. Jakob Prange                   #
# FAI Universität Augsburg           #
# SoSe 2024                          #
# Software Assignment                #
######################################
from card import Deck, Card
from database import Database


class Player:

    def __init__(self, name):
        '''
        Creates a new Player
        '''
        self.db = Database()
        self.name = name
        self.points = 0                         # value of all cards in the hand
        self.cards = []
        self.score = self.db.get_score(name)
        self.current_bet = 0

    @property
    def points(self):
        return self._points

    @points.setter
    def points(self, points):
        self._points = points

    @property
    def current_bet(self):
        return self._current_bet

    @current_bet.setter
    def current_bet(self, current_bet):
        self._current_bet = current_bet

    def get_name(self):
        '''
        :return: str
        '''
        return self.name

    def double_down(self):
        '''
        This method handles the decision of player and doubles his/her bet
        :return: None
        '''
        if self.points < 21:
            """
            print("Do you want to double your bet?")
            print("In this case you will get one card and you can not hit anymore!")
            answer = str(input())
            if answer == 'Yes':
            """
            if self.current_bet * 2 <= self.score:
                self.score -= self.current_bet          # withdraw the current bet one more time
                self.current_bet = self.current_bet * 2     # double the bet
                self.db.update_highscore(self.name, self.score)
                print(f"You places a bet of {self.current_bet} scores.")
                return True
            else:
                print(f"Not enough balance to double a bet.")
                return False
        else:
            return False


    def add_card(self, card):
        '''
        Adds one card to player's hand (list)
        :param card: Card object
        :return: None
        '''
        self.cards.append(card)
        self.points = self.sum_of_points()

    def sum_of_points(self):
        '''
        This method defines the value of all cards in the player's hand
        It also decides if Ace should be considered as 1 or 11
        :return: int
        '''
        total = 0
        ace_count = 0
        for card in self.cards:
            if card.value in ['Jack', 'Queen', 'King']:
                total += 10
            elif card.value == 'Ace':
                ace_count += 1
                total += 11
            else:
                total += int(card.value)
        while total > 21 and ace_count:
            total -= 10
            ace_count -= 1

        self._points = total
        return total

    def show_cards(self):
        '''
        This method returns a String representation of all cards in the player's hand
        :return: String
        '''
        return ', '.join(repr(card) for card in self.cards)

    def make_bet(self, amount):
        '''
        This method takes a bet from a player and withdraws an amount of a bet from user's scores
        :param amount: int
        :return: boolean
        '''
        if amount <= self.score:
            self.current_bet = amount
            self.score -= amount
            self.db.update_highscore(self.name, self.score)
            print(f"You places a bet of {amount} scores.")
            return True
        else:
            print(f"Not enough balance for {self.name} to place a bet of {amount} scores.")
            return False

    def reset_cards(self, name):
        '''
        This method helps to start a new game
        :return: None
        '''
        self.cards = []
        self.points = 0
        self.score = self.db.get_score(name)





if __name__ == '__main__':
    deck = Deck()
    deck.shuffle()
    player1 = Player('Yoshi')  # create new player
    player2 = Player('Peach')  # create new player
    while True:
        card = deck.deal()
        if player1.sum_of_points() < 16:       # suppose, that the player takes a risk
            player1.add_card(card)
        else:
            break
    while True:
        card = deck.deal()
        if player2.sum_of_points() < 16:
            player2.add_card(card)
        else:
            break

    print(player1.get_name(), "has:\n" + player1.show_cards())
    print(player2.get_name(), "has:\n" + player2.show_cards())
    print(player1.get_name(), "has " + str(player1.sum_of_points()))
    print(player2.get_name(), "has " + str(player2.sum_of_points()))
