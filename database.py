import shelve

class Database:
    '''
    Description : Class Database is a simple database for storing player's data,
    using key/value concept with shleve

    >>> db = Database()
    >>> result, message = db.new_player("Jack", "1234")
    >>> result
    True
    >>> message
    'Great! Enjoy the game, Jack!'
    >>> db.get_score("Jack")
    50.0
    >>> db.delete_player("Jack")
    True
    >>> db.close_db()

    '''

    def __init__(self, filename = "players"):
        '''
        This method initializes a DB and a file for data storage
        :param filename: file
        '''
        self.db = shelve.open(filename, writeback=True)
        self.scores_file = "scores.txt"

    def has_player(self, username):
        return username in self.db

    def new_player(self, username, password):
        '''
        s
        :param username: str
        :param password: str
        :return: boolean
        True if a new user could be created
        False if a user with this username already exists

        '''

        self.db[username] = password
        self.update_highscore(username, 50)
        self.db.sync()
        return True, (f"Great! Enjoy the game, {username}!")

    def get_score(self, username):
        '''
        This method returns a score of a particular user
        :param username: str
        :return: None or float
        '''

        #if username not in self.db:
        #    return None
        with open(self.scores_file, "r") as f:
            for line in f:
                name, score = line.strip().split()
                if name == username:
                    return float(score)
        return None

    def authentication(self, username, password):
        '''
        Checks if a password is correct for a given username
        :param username: str
        :param password: str
        :return: boolean
        True if the password for this username corresponds with the password in database
        False otherwise
        '''
        real_password = self.db.get(username)
        if real_password and real_password == password:
            return True
        return False

    def load_highscore(self):
        '''
        Loads a highscore list
        :return: list of tuples (username, score)
        '''
        scores = []
        try:
            with open(self.scores_file) as f:
                for line in f:
                    name, score = line.strip().split()
                    scores.append((name, float(score)))
        except FileNotFoundError:
            pass
        return sorted(scores, key=lambda x: x[1], reverse=True)

    def update_highscore(self, username, score):
        '''
        Adds new user to highscore list if username not found
        Or changes the score value of a particular user
        Sorts the ranking
        :param username: str
        :param score: float
        :return: None
        '''
        scores = self.load_highscore()
        for i, (name, ex_scores) in enumerate(scores):
            if name == username:
                scores[i] = (username, score)
                break
        else:
            scores.append((username, score))
        scores = sorted(scores, key=lambda x: x[1], reverse=True)
        self.save_highscore(scores)


    def save_highscore(self, scores):

        '''
        Saves a highscore list in the file ( use update_score before )
        :param score: tuple(username, score)
        :return: None
        '''
        with open(self.scores_file, "w") as f:
            for username, score in scores:
                f.write(f"{username}\t{score}\n")

    def delete_player(self, username):
        '''
        We need this method mostly for doctest, so that
        new "test-player" Jack could be deleted from database and txt file
        :param username: str
        :param password: str
        :return: boolean
        '''

        if username in self.db:
            del self.db[username]
            self.db.sync()
            scores = self.load_highscore()          # delete user from txt file
            # this method adds to the list only tuples with names, that != username
            scores = [(name, score) for name, score in scores if name != username]
            self.save_highscore(scores)
            return True
        else:
            return False

    def close_db(self):
        self.db.close()


if __name__ == "__main__":
    import doctest
    doctest.testmod()