from game import Game
from player import Player
from user_interface import UI


class Blackjack:
    def __init__(self):
        self.ui = UI()
        self.game = self.ui.game

    def start_game(self):
        highscore = self.game.db.load_highscore()
        print(highscore)
        username = input("Enter your username: ")
        if self.game.db.has_player(username):
            password = input("Enter your password: ")
            result = self.game.db.authentication(str(username), password)
            if not result:
                print("Wrong password!")
                self.game.finish_game()
            else:
                answer = input("Do you want to load the game (L) or start a new one (S)?")
                if answer == "L":
                    print("Loading the last game...")
                    self.game = self.game.load_game(username)
                    self.game.run_if_loaded()
                    print("Thank you for playing!")
                else:
                    player = Player(username)
                    print("Starting a new game of Black Jack...")
                    #self.game = Game()
                    self.game.run(player)
                    print("Thank you for playing!")
        else:
            password = input("Set your password: ")
            self.game.db.new_player(username, password)
            player = Player(username)
            print("Starting a new game of Black Jack...")
            print("Thank you for playing!")

    def main(self):
        self.start_game()


if __name__ == "__main__":
    test = Blackjack()
    #test.main()
