Main Objective: Get your Hand value as close to 21 as possible, your current Hand value is represented above your Card-Deck after 'Points:'.
Opponent: Your Opponent is the Dealer and therefore the Casino, get closer to 21 than the Dealer to win some Money

First: Before Cards get delt, place your Bets, everybody starts with 50 Money
Second: Choose an Action with the following Buttons:
    'Hit': Get another Card form the Dealer as often as you want, but be careful, don't let your Deck get over 21!
    'Stand': Tell the Dealer you don't want more cards, now you must wait until the Round is over to find out if you won.
    'Double Down': Place your starter Bet again, you will only get one more Card that can win you the Round.
    'Surrender': After getting 2 Cards you can surrender and prematurely end the round. Get half your Bet back.
Last: The Dealer reveals his Deck and optionally draws some cards and compares his Hand value with yours.

Win Conditions:
Your Hand value is 21 and the Dealer's not: get 2x your placed Bet
Your Hand value is greater than the Dealers: get 1.5x your placed Bet
Your Hand value and the Dealer's Hand value is 21: get back 1x your placed Bet

Values of a Card:
For all number Cards, its Value is displayed on the Card.
'Jack','Queen','King': Counts as 10
'Ace': Can count as 11 or 1, the game auto-evaluates this