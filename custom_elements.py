import tkinter as tk


def set_default_stats(widget):
    try:
        if widget.cget("bg") == "SystemButtonFace":
            widget.config(bg="White")
        if widget.cget("fg") == "SystemButtonText":
            widget.config(fg="Black")
    except(tk.TclError):
        pass


class Button(tk.Button):
    def __init__(self, name, value, *args, **kwargs):
        tk.Button.__init__(self, *args, **kwargs)
        self.name = tk.StringVar(name="ID", value=name).get
        self.value = tk.IntVar(name="Value", value=value).get()
        self.color = self.cget("fg")
        self.tip = ""
        self.tip_window = None
        self.bind("<Enter>", self.on_enter)
        self.bind("<Leave>", self.on_leave)
        self.config(disabledforeground="red")
        set_default_stats(self)

    def updateSomeLabel(self, label, text):
        label.updateText(text)

    def toggle(self, value: int):
        if self['state'] == 'disabled' and not value == 0:
            self.configure(state='active')
        else:
            self.configure(state='disabled')

    def on_enter(self, event):
        if self.tip != "":
            self.show_tip(event)
        self.configure(fg="dodger blue")

    def on_leave(self, event):
        if self.tip != "":
            self.hide_tip(event)
        self.configure(fg=self.color)

    def show_tip(self, event):
        self.tip_window = tk.Toplevel(self)
        self.tip_window.wm_overrideredirect(True)
        x = self.winfo_width() + self.winfo_rootx()
        y = event.y + self.winfo_rooty()
        self.tip_window.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tip_window, text=self.tip, relief=tk.SOLID, font="Arial 14", background="antique white")
        label.pack(ipadx=1)
        self.tip_window.attributes("-topmost", True)

    def hide_tip(self, event):
        if self.tip_window:
            self.tip_window.attributes("-topmost", False)
            self.tip_window.destroy()
            self.tip_window = None

    def create_tip(self, message):
        self.tip = message

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value


class Label(tk.Label):
    def __init__(self, name, value, *args, **kwargs):
        tk.Label.__init__(self, *args, **kwargs)
        self.name = tk.StringVar(value=name, name="ID").get()
        self.value = tk.IntVar(name="Value", value=value).get()
        set_default_stats(self)

    def updateText(self, string):
        self.config(text=string)
        self.update()


class Entry(tk.Entry):
    def __init__(self, name, *args, **kwargs):
        tk.Entry.__init__(self, *args, **kwargs)
        self.name = tk.StringVar(value=name, name="ID").get()
        set_default_stats(self)


class Frame(tk.Frame):
    def __init__(self, name, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.name = tk.StringVar(value=name, name="ID").get()
        set_default_stats(self)
