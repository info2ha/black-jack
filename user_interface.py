######################################
# Introduction to Python Programming #
# Prof. Dr. Annemarie Friedrich      #
# Dr. Jakob Prange                   #
# FAI Universität Augsburg           #
# SoSe 2024                          #
# Software Assignment                #
######################################
import tkinter as tk
import card
import database as db
import game
import player as p
import custom_elements as cE
from time import sleep


class UI:
    '''
    Class for any inputs and outputs.
    '''
    row_minsize = 80
    col_minsize = 20
    button_width = 22
    button_font = ("Arial", 13)
    enable_experimental_features = False  # TESTING OPTION

    def __init__(self):
        '''
        Creates a new user interface
        '''
        self.game = game.Game(self)  # current game session
        self.dealer = None  # displayable UI dealer object
        self.sidebets = None  # displayable sidebets
        self.currentPlayer = None  # current displayable UI player object
        self.players = []  # contains all displayable UI player objects
        self.display = None  # tkinter main window
        self.setup_main_window()
        self.currentInput = None  # current input from player
        self.database = db.Database()
        LoginWindow(self.display, self.database, self, self.setup_intro_window())
        self.showLayout = False  # debugging option
        self.loginOpen = False
        self.rulesOpen = False
        self.scoreboardOpen = False
        self.scoreboardWindow = None  # tkinter scoreboard window
        self.ruleWindow = None  # tkinter rule window
        self.currentHand = None  # current hand of a player
        self.count_hand = 0  # remembers if player plays with main (0) or spilt hand(1)
        self.ready = False
        while True:
            self.wait_for_input()

    def restart(self):  # deprecated
        """
        Resets the user interface
        """
        try:
            current_hand = self.currentPlayer.cardContainer[self.count_hand]
            childs = self.display.winfo_children()
            for child in childs:
                if type(child) is cE.Label:
                    if child.name == "win" or child.name == "loss":
                        child.destroy()
                pass
            sleep(3)
            """Code in UI"""
            self.game.players[1].reset_cards(self.currentPlayer.name)
            current_hand.empty()  # Code in UI to clear all displayed cards for player
            self.dealer.empty()  # Code in UI to clear all displayed cards for dealer
            current_hand.finished = False  # Code in UI
            self.currentPlayer.button_check.toggle(1)  # Code in UI
            self.currentPlayer.bet = 0  # Code in UI
            self.game.deck = card.Deck()
            self.currentPlayer.bet_label.updateText("")  # Code in UI to reset bet display
            self.currentPlayer.points_label.updateText("Points: " + str(self.currentPlayer.player.sum_of_points()))  # updates UI

            self.ready = False  # Code in UI
        except AttributeError:
            pass

    def switch_player(self):
        """
        Switches the current player to the next player
        """
        count = len(self.players)
        if count == 1:
            self.currentPlayer = self.players[0]
            print("Setup: Only One Player")
        else:
            pass  # Extend for multiple Players

    def switch_hand(self):
        """
        Switches between two hands of a player
        """
        if self.currentPlayer is not None:
            if len(self.currentPlayer.cardContainer) == 2:
                if self.count_hand == 1:
                    self.currentHand = self.currentPlayer.cardContainer[1]
                    self.currentHand.frame.configure(highlightbackground="white",
                                                     highlightthickness=0)
                    self.currentPlayer.cardContainer[0].frame.configure(highlightbackground="orange red",
                                                                        highlightthickness=3)
                    self.count_hand = 0
                    print("Split hand")
                elif self.count_hand == 0:
                    self.currentHand = self.currentPlayer.cardContainer[0]
                    self.currentHand.frame.configure(highlightbackground="white",
                                                     highlightthickness=0)
                    self.currentPlayer.cardContainer[1].frame.configure(highlightbackground="orange red",
                                                                        highlightthickness=3)
                    self.count_hand = 1
                    print("Main hand")
            else:
                self.count_hand = 0
                self.currentHand = self.currentPlayer.cardContainer[0]

    def toggle_sidebets(self):
        if self.sidebets.winfo_viewable():
            self.sidebets.grid_forget()
        else:
            self.sidebets.grid(row=1, column=5, padx=5, pady=0, columnspan=1, rowspan=3)

    def toggle_all_playerbuttons(self, value: int):
        """
        Toggles all the buttons on the user interface
        value: 0 for disabled, 1 for enabled
        """
        self.currentPlayer.button_double.toggle(value)
        self.currentPlayer.button_hit.toggle(value)
        self.currentPlayer.button_stand.toggle(value)
        self.currentPlayer.button_surrender.toggle(value)
        self.currentPlayer.button_split.toggle(value)

    def if_won(self):
        win = cE.Label("win", 1, self.display, text="Game Over, you won!\nLook into Console", font=("Arial", 30),
                       fg="green")  # Displays Win Text
        win.place(relx=0.325, rely=0.5)
        self.game.delete_game(self.currentPlayer.name)
        self.ready = False
        self.display.update()
        self.restart()

    def if_lost(self):
        loss = cE.Label("loss", 0, self.display, text="Game Over, you lost!\nLook into Console", font=("Arial", 30),
                        fg="red")  # Displays Loss Text
        loss.place(relx=0.325, rely=0.5)
        self.game.delete_game(self.currentPlayer.name)
        self.ready = False
        self.display.update()
        self.restart()

    def load_game_into_ui(self):
        self.game = game.Game.load_game(self.currentPlayer.player.name, self)
        self.setup_dealer()
        for card in self.game.players[0].cards:
            self.dealer.add_card(card, True)
        for card in self.game.players[1].cards:
            self.currentPlayer.add_card(card, self.count_hand)
        self.dealer.flip_card(self.dealer.cards[0], 0)
        self.currentPlayer.bet_label.updateText(str(self.game.players[1].current_bet))
        self.currentPlayer.points_label.updateText("Points: " + str(self.game.players[1].sum_of_points()))  # updates UI
        self.currentPlayer.bet = self.game.players[1].current_bet
        self.ready = True
        self.display.deiconify()
        self.display.update()

    def wait_for_input(self):
        """
        Waits until the user inputs something and does input logic
        """
        try:
            self.display.update()
            container = self.currentPlayer
            if self.currentInput == "run" and not self.ready:  # Do something if player placed a bet
                if container.player.make_bet(container.bet):
                    self.ready = True
                    container.button_check.toggle(0)
                    self.game.dealers_turn()
                    self.game.players_turn(container.player)
                    container.points_label.updateText("Points: " + str(container.player.sum_of_points()))  # updates UI
                    self.currentPlayer.bet_label.updateText(str(self.game.players[1].current_bet))
                else:
                    container.bet = 0
                    self.currentPlayer.bet_label.updateText(str(self.game.players[1].current_bet))
                self.currentInput = None
            if container is not None and len(container.cardContainer) > 0 and self.ready:
                current_hand = container.cardContainer[self.count_hand]
                if self.currentInput == "split" and not current_hand.finished and current_hand.cards[0].prop.value == current_hand.cards[1].prop.value:
                    # Do something if player wants to split
                    split_hand = CardContainer(self.display, container)
                    container.cardContainer.append(split_hand)
                    split_hand.frame.grid_configure(row=3, column=1, padx=5, pady=0, columnspan=3, rowspan=1)
                    split_hand.add_card(current_hand.prop, False)
                    split_hand.draw()
                    self.currentPlayer.button_split.config(state=tk.DISABLED)  # deactivates button
                    self.currentInput = None
                if current_hand.cardCount >= 1 and not current_hand.finished:
                    if self.currentInput == "hit":  # Do something if player presses Hit
                        self.game.hit()
                        container.points_label.updateText("Points: " + str(container.player.sum_of_points()))  # updates UI
                        self.switch_hand()
                        self.currentInput = None
                    elif self.currentInput == "stand":  # Do something if player presses stand
                        current_hand.finished = True  # Marks current hand for disabling
                        self.game.win_condition()
                        self.currentInput = None
                        self.switch_hand()
                    elif self.currentInput == "double":  # Do something if player presses double down
                        if container.player.double_down():
                            container.add_bet(container.bet)  # places bet inside the UI
                            container.bet_label.updateText(str(container.bet))  # updates UI
                            self.game.hit()
                            container.points_label.updateText("Points: " + str(container.player.sum_of_points()))  # updates UI
                            current_hand.finished = True  # Marks current hand for disabling
                            self.game.win_condition()
                            self.switch_hand()
                            self.currentInput = None
                        else:
                            print("Table: Not enough Money in Bank")
                            self.currentInput = None
                    elif self.currentInput == "surrender":  # Do something if player presses surrender
                        if current_hand.cardCount == 2:  # can only surrender at two cards
                            self.restart()
                            self.currentInput = None
                        else:
                            print("Table: Surrender only at two Cards")
                            self.currentInput = None
                else:
                    self.currentInput = None
        except AttributeError:
            pass
    def exit_game(self):  # when player wants to go back to main menu
        if self.ready:
            self.game.save_game(self.currentPlayer.player.name)
        self.display.destroy()  # Destroys current instance
        UI()  # Open a new instance

    def setup_main_window(self):
        '''
        Generates the main Window with a 5x5 Grid
        '''
        self.display = tk.Tk()
        self.display.title('Black Jack')
        self.display.resizable(False, True)
        self.display.geometry("1100x800")
        self.display.configure(background='white')
        self.display.protocol("WM_DELETE_WINDOW", self.exit_game)  # get back to main menu if window is closed
        if UI.enable_experimental_features:
            self.setup_sidebets()
        for column in range(5):
            self.display.grid_columnconfigure(column, minsize=self.col_minsize, weight=1)
        for row in range(3):
            self.display.grid_rowconfigure(row, minsize=self.row_minsize, weight=1)
        self.display.grid_rowconfigure(3, minsize=self.row_minsize + 30, weight=1)
        self.display.grid_rowconfigure(4, minsize=self.row_minsize + 60, weight=1)
        self.display.grid_propagate(True)
        self.display.withdraw()  # hides the Main Window

    def setup_rule_window(self):
        '''
        Draw and create a Rule Window displaying game rules
        '''
        if not self.rulesOpen:
            frame = tk.Toplevel()
            self.ruleWindow = frame
            frame.withdraw()
            frame.deiconify()
            scrollbar = tk.Scrollbar(frame)
            text = tk.Text(frame, wrap="word", font=("Aptos Display", 14), yscrollcommand=scrollbar.set)
            text.pack(expand=True, fill=tk.BOTH)

            self.rulesOpen = True
            with open("Rules.txt", "r") as f:
                content = f.read()
                text.delete(1.0, tk.END)  # clear previous content
                text.insert(tk.END, content)  # insert new content
                text.configure(state="disabled")  # disable user input inside the text element
        else:
            self.rulesOpen = False
            self.ruleWindow.destroy()

    def setup_scoreboard_window(self):
        '''
        Draw and create a Scoreboard Window displaying a Scoreboard
        '''
        if not self.scoreboardOpen:
            frame = tk.Toplevel()
            self.scoreboardWindow = frame
            frame.withdraw()
            frame.deiconify()
            frame.configure(background="black")
            text0 = tk.Text(frame, width=30, wrap="none", font=("Arial Black", 16), borderwidth=1, relief="sunken")
            text1 = tk.Text(frame, wrap="none", width=20, font=("Arial Black", 16), borderwidth=1, relief="sunken")
            highscores = self.load_highscores()
            text0.insert(1.0, "Username\n")
            text1.insert(1.0, "Highscore\n")
            text0.insert(2.0, "-" * 61 + "\n")
            text1.insert(2.0, "-" * 40 + "\n")
            i = 3.1
            for score in highscores:
                text0.insert(i, "-" * 61 + "\n")
                text0.insert(i, str(score[0]) + "\n")
                text1.insert(i, "-" * 41 + "\n")
                text1.insert(i, str(score[1]) + "\n")
                i += 2.0
            text0.configure(state="disabled", highlightthickness=1, highlightcolor="black")
            text1.configure(state="disabled", highlightthickness=1, highlightcolor="black")
            text0.pack(expand=False, side=tk.LEFT, fill=tk.BOTH)
            text1.pack(expand=False, side=tk.LEFT, fill=tk.BOTH)
            self.scoreboardOpen = True
        else:
            self.scoreboardOpen = False
            self.scoreboardWindow.destroy()

    def setup_intro_window(self):
        '''
        draw and create an Intro Window that displays the main menu
        '''
        frame = tk.Toplevel()
        frame.geometry("500x350")
        frame.resizable(False, False)
        label_1 = tk.Label(frame, text="Black Jack", font=("Algerian", 60))
        b_newgame = cE.Button("newgame", 0, frame, text="New Game", width=10, font=("Impact", 30)
                              , command=lambda: [self.display.deiconify(), b_newgame.toggle(0), b_loadgame.toggle(0)])
        b_loadgame = cE.Button("loadgame", 0, frame, text="Load Game", width=10, font=("Impact", 30)
                               , command=lambda: [self.load_game_into_ui(), b_loadgame.toggle(0), b_newgame.toggle(0)])

        b_scores = cE.Button("scores", 0, frame, text="Highscores", command=self.setup_scoreboard_window, width=10,
                             font=("Impact", 30))
        b_rules = cE.Button("rules", 0, frame, text="Rules", command=self.setup_rule_window, width=10,
                            font=("Impact", 30))
        label_1.pack()
        b_newgame.place(relx=0.05, rely=0.3, anchor="nw")
        b_loadgame.place(relx=0.55, rely=0.3, anchor="nw")
        b_scores.place(relx=0.05, rely=0.6, anchor="nw")
        b_rules.place(relx=0.55, rely=0.6, anchor="nw")
        frame.attributes("-topmost", True)
        return frame

    def setup_dealer(self):
        """
        Sets up a displayable dealer
        """
        dealerCards = CardContainer(self.display, self.game.players[0])
        dealerCards.frame.grid(row=0, column=1, padx=5, pady=0, columnspan=3, rowspan=1)
        self.dealer = dealerCards

    def setup_sidebets(self):

        sidebets = cE.Frame("sidebets", self.display, highlightbackground="dodger blue", highlightthickness=3)
        sidebets.grid(row=0, column=5, padx=5, pady=0, columnspan=1)
        self.sidebets = sidebets
        button_toggle = cE.Button("toggle_sidebets", 0, self.display, text=">\nS\nI\nD\nE\nB\nE\nT\nS\n>", font=("Arial Black", 15), command=self.toggle_sidebets,
                                  width=2, height=12, relief="flat", fg="dodger blue")
        button_toggle.place(x=1085, rely=0.46, anchor=tk.CENTER)
        button_toggle.create_tip("Open sidebets")
        for i in range(9):  # generates placeholders
            button = cE.Button("bet" + str(i), i, sidebets, text="Bet " + str(i + 1) + " Ratio 5:1", font=UI.button_font, width=UI.button_width, relief="solid")  # sample Bets
            button.pack(padx=10, pady=10, side=tk.TOP)
            button.create_tip("Here is a Tip Description")
        sidebets.grid_forget()

    def load_highscores(self):
        return self.database.load_highscore()

    def add_highscore(self, name, score):
        self.database.update_highscore(name, score)

    def set_current_input(self, string):
        self.currentInput = string


class PlayerContainer:
    '''
    Define a Container containing all UI elements that are displayed for an individual player
    '''

    def __init__(self, display, player, ui):
        self.display = display  # tkinter main window
        self.content = []
        self.ui = ui  # current UI
        self.cardContainer = []  # a display for cards Index 0 = Main Hand, Index 1 = Split Hand
        self.player = player  # player that belongs to this display
        self.name = player.name
        self.points = player.points
        self.bet = player.current_bet
        """Buttons for user input"""
        self.button_hit = None
        self.button_stand = None
        self.button_double = None
        self.button_surrender = None
        self.button_split = None
        self.button_check = None
        """Buttons for user input"""
        self.points_label = None  # label that displays player deck value
        self.bet_label = None  # label that displays the current bet value
        self.score = ui.database.get_score(self.name)  # player highscore
        print(self.score)
        print(ui.database.get_score(self.name))
        self.frame = self.setup_player_container()  # tkinter frame containing all UI elements

    @property
    def bet(self):
        return self._bet

    @bet.setter
    def bet(self, value):
        self._bet = value

    def setup_player_container(self):
        '''
        Set up a PlayerContainer (display) for a Player
        '''
        player_frame = cE.Frame("PlayerContainer", self.display)
        player_frame.grid_propagate(True)
        player_frame.configure(highlightbackground="black", highlightthickness=2)
        player_frame.grid(row=4, column=0, padx=5, pady=5, rowspan=1, columnspan=5)
        '''Setup elements of top row'''
        playerLabel = cE.Label("playerText", 0, player_frame, text="Player: " + self.name[:8], font=("Arial", 15),
                               width=UI.col_minsize)
        playerLabel.grid(row=0, column=1, pady=5)
        valueLabel = cE.Label("pointsValue", self.points, player_frame,
                              text="Points: " + str(self.player.sum_of_points()),
                              font=("Arial", 15), width=UI.col_minsize)
        valueLabel.grid(row=0, column=3, pady=5)
        self.points_label = valueLabel
        bSurr = cE.Button("buttonSurrender", 0, player_frame, text="Surrender", width=UI.button_width,
                          font=UI.button_font, command=(lambda: self.ui.set_current_input("surrender")))
        bSurr.grid(row=0, column=2)
        self.button_surrender = bSurr  # assign input button to UI
        # End
        '''Setup display and Gui elements of right side'''
        button_frame = cE.Frame("CardActions", player_frame)
        if self.ui.showLayout:
            button_frame.configure(highlightbackground="black", highlightthickness=1)
        bHit = cE.Button("buttonHit", 0, button_frame, text="Hit", width=UI.button_width, font=UI.button_font,
                         command=(lambda: self.ui.set_current_input("hit")))
        bStand = cE.Button("buttonStand", 0, button_frame, text="Stand", width=UI.button_width, font=UI.button_font,
                           command=(lambda: self.ui.set_current_input("stand")))
        bDouble = cE.Button("buttonDouble", 0, button_frame, text="Double Down", width=UI.button_width,
                            font=UI.button_font, command=lambda: self.ui.set_current_input("double"))
        bSplit = cE.Button("buttonSplit", 0, button_frame, text="Split", width=UI.button_width,
                           font=UI.button_font, command=lambda: self.ui.set_current_input("split"))
        bHit.pack(padx=2, pady=5, side=tk.TOP)
        bStand.pack(padx=2, pady=5, anchor="n")
        bDouble.pack(padx=2, pady=5, anchor="n")
        bSplit.pack(padx=2, pady=5, anchor="n")
        button_frame.grid(row=1, column=4, padx=5, pady=5)
        self.button_hit = bHit  # assign input button to UI
        self.button_stand = bStand  # assign input button to UI
        self.button_double = bDouble  # assign input button to UI
        self.button_split = bSplit  # assign input button to UI
        self.button_hit.create_tip("Draw one card")
        self.button_stand.create_tip("Finish deck")
        self.button_double.create_tip("Doubles your Bet!\nDraw one last card")
        self.button_split.create_tip("Split your cards")
        self.button_surrender.create_tip("Surrender your current cards\nBegin new round")
        if not UI.enable_experimental_features:
            self.button_split.toggle(1)
        # End
        '''Setup display and chips and Gui elements of left side'''
        chip_frame = cE.Frame("ChipActions", player_frame)
        if self.ui.showLayout:
            button_frame.configure(highlightbackground="black", highlightthickness=1)
        chips = []
        chip1 = cE.Button("chip1", 5, chip_frame, text="5", bg="burlywood2")
        chip1.configure(command=lambda: [self.add_bet(chip1.value), chip1.updateSomeLabel(dispLabel, self.bet)])
        chips.append(chip1)
        chip2 = cE.Button("chip2", 10, chip_frame, text="10", bg="DarkOrange1")
        chip2.configure(command=lambda: [self.add_bet(chip2.value), chip2.updateSomeLabel(dispLabel, self.bet)])
        chips.append(chip2)
        chip3 = cE.Button("chip3", 25, chip_frame, text="25", bg="SpringGreen4")
        chip3.configure(command=lambda: [self.add_bet(chip3.value), chip3.updateSomeLabel(dispLabel, self.bet)])
        chips.append(chip3)
        chip4 = cE.Button("chip4", 50, chip_frame, text="50", bg="firebrick2")
        chip4.configure(command=lambda: [self.add_bet(chip4.value), chip4.updateSomeLabel(dispLabel, self.bet)])
        chips.append(chip4)
        chip5 = cE.Button("chip5", 100, chip_frame, text="100", bg="deep sky blue")
        chip5.configure(command=lambda: [self.add_bet(chip5.value), chip5.updateSomeLabel(dispLabel, self.bet)])
        chips.append(chip5)
        chip6 = cE.Button("chip6", 500, chip_frame, text="500", bg="magenta4")
        chip6.configure(command=lambda: [self.add_bet(chip6.value), chip6.updateSomeLabel(dispLabel, self.bet)])
        chips.append(chip6)
        count = 0
        row = 0
        for chip in chips:
            print("Setup: Placed chip, Value:", chip.value)
            if count % 2 == 0:
                row += 1
            chip.grid(column=count % 2, row=row)
            count += 1
            chip.configure(font=UI.button_font, width=10, padx=1, pady=1)
        for column in range(2):
            chip_frame.grid_columnconfigure(column, minsize=50, weight=1)
        for row in range(1, 3):
            chip_frame.grid_rowconfigure(row, minsize=20, weight=1)
        chip_frame.grid_rowconfigure(0, minsize=40, weight=1)
        betLabel = cE.Label("currentBetText", 5, player_frame, text="current Bet:", font=("Arial", 15))
        dispLabel = cE.Label("currentBetValue", self.bet, player_frame, text="", font=("Arial", 15), relief="solid", width=10)
        placedCheck = cE.Button("isBetPlaced", 0, player_frame, text="✔", font=("Arial Black", 17), relief="flat", width=1,
                                command=lambda: self.ui.set_current_input("run"),bg="white")
        self.button_check = placedCheck
        placedCheck.create_tip("Confirm bet and start game")
        dispLabel.place(relx=0.1, rely=0.2, anchor="n")
        placedCheck.place(relx=0.17, rely=0.16, anchor="n")
        betLabel.grid(row=0, column=0, pady=5)
        self.bet_label = dispLabel  # assign label to UI
        saldoLabel = cE.Label("currentSaldo", self.score, player_frame, text="$: " + str(self.score),
                              font=("Arial", 15), width=UI.col_minsize)
        saldoLabel.grid(row=0, column=4, pady=5)
        chip_frame.grid(row=1, column=0, padx=5, pady=5)
        self.add_elements(chips)
        self.cardContainer.append(CardContainer(player_frame, self.name))  # adds the main card display to the player container
        self.add_elements(self.cardContainer[0])
        self.add_elements([playerLabel, valueLabel, bHit, bStand, bDouble, bSurr, betLabel, dispLabel, placedCheck,saldoLabel])
        self.ui.switch_hand()
        return player_frame  # returns the tkinter frame

    def add_bet(self, value):
        """
        Display the bet on the table
        """
        if not self.ui.ready:
            self.bet += value

    def draw(self):
        """
        Draws all card into the card display
        """
        for cardContainer in self.cardContainer:
            cardContainer.draw()

    def add_card(self, card: card.Card, index):
        """
        Adds a card to the card display, index: 0 = mainhand, 1=splithand
        """
        print(self.ui.count_hand)
        self.cardContainer[index].add_card(card, False)
        self.points_label.updateText("Points: " + str(self.player.sum_of_points()))

    def add_elements(self, elements):
        """
        Add a list of elements or an element to container
        """
        if isinstance(elements, list):
            for e in elements:
                if e not in self.content:
                    self.content.append(e)
                    print("Setup: Element auto-added to PlayerContainer")
        else:
            self.content.append(elements)

    def search_element(self, type, name):
        """Types: cE.Button, cE.Label,CardContainer; Name: Widgetname"""
        for e in self.content:
            if isinstance(e, type):
                if e.name == name:
                    return e
        print("Error: Couldn't find Element with:" + name + " or no Name Value assigned")

    def get_elements(self, type):
        """Type: cE.Button, cE.Label, cE.Frame; Returns list of all Elements of the appropriate type"""
        list = []
        for element in self.content:
            if isinstance(element, type):
                list.append(element)
        return list


class CardContainer:
    '''
    Displays the last 3 cards a player has been delt
    '''

    def __init__(self, display, player: p.Player):
        self.display = display  # tkinter main window
        self.player = player
        self.cardCount = 0
        self.frame = self.setup_card_container()  # tkinter frame containing all UI elements
        self.cards = []  # list of all cards
        self.finished = False

    def setup_card_container(self):
        '''
        Generates the card display with a 1x3 Grid
        '''
        cardContainer = cE.Frame("CardContainer", self.display)  # Contains all displayed cards of a player or the dealer
        cardContainer.grid(row=1, column=1, padx=0, pady=0, rowspan=1, columnspan=3)
        for column in range(3):
            cardContainer.columnconfigure(column, minsize=UI.col_minsize, weight=1)
        return cardContainer  # returns tkinter frame

    def add_card(self, card, flipped):
        '''
        Convert card object into UICard and adds it to the container
        '''
        if self.cardCount < 11:  # max count of cards a player can have
            if not isinstance(card, UICard):
                converted_card = UICard(self.cardCount % 3, 0, card, self.frame)  # converts card and calculates position
                if converted_card not in self.cards:
                    if flipped:
                        converted_card.flip(1)  # flips the card
                    self.cards.append(converted_card)  # add card to the container
                    self.cardCount += 1
                    self.draw()  # draw and display a card
                    print("Setup: Card converted and added")

    def get_cards(self):
        return self.cards

    def flip_card(self, card, value: int):
        """
        Flips a card; 0 for unflipped, 1 for flipped
        """
        card.flip(value)

    def empty(self):
        """
        Clears all displayed cards
        """
        for child in self.frame.winfo_children():
            child.destroy()
        self.cardCount = 0
        self.cards.clear()

    def draw(self):
        """
        Draws all cards in the card container onto the display
        """
        for card in self.cards:
            card.draw()


class UICard:
    """
    Represents one displayable card
    """

    def __init__(self, column, row, prop, cardContainer):
        self.column = column
        self.row = row
        self.prop = prop  # card object corresponding to the UICard
        self.cardName = self.generate_card_descriptor()  # generates a representation from the card object value
        self.cardSuite = self.generate_card_suite()  # generates a representation from the card object suite
        self.cardContainer = cardContainer  # display the card is in
        self.flipped = False

    def draw(self):
        """
        Draws and setups a card on the display
        """
        self.frame = cE.Frame("Card" + self.cardName, self.cardContainer,
                              container=False,
                              bg=self.cardSuite,
                              borderwidth=5, relief="sunken")
        self.frame.grid(row=self.row, column=self.column, padx=10)
        if not self.flipped:
            fgColor = "white"
        else:
            fgColor = self.cardSuite
        label1 = tk.Label(self.frame, text=self.cardName, font=("Arial", 20), bg=self.cardSuite, fg=fgColor)
        label1.grid(row=0, column=0)
        label2 = tk.Label(self.frame, text=self.cardName, font=("Arial", 20), bg=self.cardSuite, fg=fgColor)
        label2.grid(row=2, column=1)
        label3 = tk.Label(self.frame, text=self.cardName, font=("Arial", 20), bg=self.cardSuite, fg=fgColor)
        label3.grid(row=4, column=2)
        for i in range(3):
            self.frame.grid_columnconfigure(i, minsize=40)
        self.frame.grid_rowconfigure(1, minsize=20)
        self.frame.grid_rowconfigure(3, minsize=20)

    def generate_card_descriptor(self):
        """
        Generates what value is displayed on the card
        """
        match self.prop.value:
            case 'Jack':
                return "J"
            case 'Queen':
                return "Q"
            case 'King':
                return "K"
            case 'Ace':
                return "A"
            case _:
                return str(self.prop.value)

    def generate_card_suite(self):
        """
        Generates the suite that is displayed on the card
        """
        match self.prop.suit:
            case 'Diamonds':
                return "dodger blue"
            case 'Spades':
                return "grey12"
            case 'Hearts':
                return "dodger blue"
            case 'Clubs':
                return "grey12"
            case _:
                return "dodger blue"

    def flip(self, value):
        """
        Flips a card
        """
        if self.flipped and value == 0:
            self.flipped = False
            self.draw()
        elif not self.flipped and value == 1:
            self.flipped = True
            self.draw()

    @property
    def frame(self):
        return self._frame

    @frame.setter
    def frame(self, frame):
        self._frame = frame


class LoginWindow:
    """
    Creates a login window
    """

    def __init__(self, display, database, ui, previousWindow):
        previousWindow.withdraw()
        self.database = database
        self.display = display  # tkinter main window
        self.previousWindow = previousWindow  # the window that opened the login window
        self.top = tk.Toplevel()  # new tkinter window
        self.top.title('Login Window')
        self.top.attributes("-topmost", True)
        self.ui = ui
        """Username field"""
        uLabel = tk.Label(self.top, text="Username", font=UI.button_font)
        uLabel.pack(pady=5)
        self.user = tk.Entry(self.top, font=UI.button_font)
        self.user.pack(pady=5, padx=20)
        """Password Field"""
        pLabel = tk.Label(self.top, text="Password", font=UI.button_font)
        pLabel.pack(pady=5)
        self.pw = tk.Entry(self.top, show="*", font=UI.button_font)
        self.pw.pack(pady=5, padx=20)
        """Feedback Label"""
        self.fLabel = cE.Label("feedbackText", 0, self.top, font=UI.button_font, fg="red")
        self.fLabel.pack(pady=5)
        """Login Button"""
        blogin = tk.Button(self.top, text="Login", command=self.login, font=UI.button_font)
        blogin.pack(pady=20)

    def login(self):
        """
        Login function
        """
        username = self.user.get()
        password = self.pw.get()
        if username and password:
            conf, message = self.database.new_player(username, password)
            if conf:
                self.previousWindow.deiconify()
                self.top.destroy()
                self.display.attributes("-topmost", True)
                self.setup_player(username)
            else:
                conf = self.database.authentication(username, password)
                if not conf:
                    self.fLabel.updateText("Wrong Password!")
                else:
                    self.previousWindow.deiconify()
                    self.top.destroy()
                    self.display.attributes("-topmost", True)
                    self.setup_player(username)  # creates the player in game and inside UI

    def setup_player(self, username):
        """
        Creates a player object inside game and the UI
        """
        player = p.Player(username)
        pContainer = PlayerContainer(self.display, player, self.ui)
        self.ui.players.append(pContainer)
        self.ui.switch_player()


if __name__ == '__main__':
    '''
    write additional testing code here for things that don't work well as unit tests:
    '''
    ui = UI()
