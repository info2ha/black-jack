######################################
# Introduction to Python Programming #
# Prof. Dr. Annemarie Friedrich      #
# Dr. Jakob Prange                   #
# FAI Universität Augsburg           #
# SoSe 2024                          #
# Software Assignment                #
######################################

from database import Database
from card import Deck, Card
from player import Player


class Game:

    def __init__(self, ui):
        '''
        Creates a new game session. Think about which pieces of information you need at this time
        (e.g. players, game mode, time limit, ...) and add them as arguments.
        '''
        self.deck = Deck()
        self.players = []
        self.db = Database()
        self.saved_games = "saved_games.txt"
        self.ui = ui

    def add_player(self, player: Player):
        '''
        Adds a Player object as a player for the current game session.
        :param player: an Object of a Player class
        :return: None
        '''
        self.players.append(player)

    def hit(self):
        #while True:
        #print("Do you want to take one more card?\nYes/No")  # hitting
        #answer = str(input())
        #if answer == 'Yes'
        if self.players[1].points < 21:
            card = self.deck.deal()
            self.ui.currentPlayer.add_card(card, False)
            self.players[1].add_card(card)
            self.ui.currentPlayer.points_label.updateText("Points: " + str(self.players[1].sum_of_points()))
            print("You cards:", self.players[1].show_cards())
        else:
         self.win_condition()

    def save_game(self, username):
        '''
        This method saves a current game in a file
        :param username: str
        :return: None
        '''
        game_data = {}
        # Read data from file
        try:
            with open(self.saved_games, 'r') as file:
                lines = file.readlines()
                current_user = None
                for line in lines:
                    line = line.strip()
                    if line.startswith("User:"):
                        current_user = line.split("User:")[1].strip()
                        game_data[current_user] = {"player_cards": [], "dealer_cards": [], "bet": 0}
                    elif current_user:
                        if line.startswith("Player Cards:"):
                            game_data[current_user]["player_cards"] = line.split("Player Cards:")[1].strip().split(
                                ", ")
                        elif line.startswith("Dealer Cards:"):
                            game_data[current_user]["dealer_cards"] = line.split("Dealer Cards:")[1].strip().split(
                                ", ")
                        elif line.startswith("Bet:"):
                            game_data[current_user]["bet"] = int(line.split("Bet:")[1].strip())
        except FileNotFoundError:
            pass

        game_state = {
            "player_cards": [repr(card) for card in self.players[1].cards],
            "bet": self.players[1].current_bet,
            "dealer_cards": [repr(card) for card in self.players[0].cards]
        }
        game_data[username] = game_state

        # Write data into the file
        with open(self.saved_games, 'w') as file:
            for user, data in game_data.items():
                file.write(f"User: {user}\n")
                file.write(f"Player Cards: {', '.join(data['player_cards'])}\n")
                file.write(f"Dealer Cards: {', '.join(data['dealer_cards'])}\n")
                file.write(f"Bet: {data['bet']}\n")

    @classmethod
    def load_game(cls, username, ui, filename="saved_games.txt") -> 'Game':
        '''
        This classmethod loads a game from a file
        :param username: str
        :return: game
        '''
        game_data = {}
        current_user = None

        try:
            with open(filename, 'r') as file:
                lines = file.readlines()
                for line in lines:
                    line = line.strip()
                    if line.startswith("User:"):
                        current_user = line.split("User:")[1].strip()
                        game_data[current_user] = {"player_cards": [], "dealer_cards": [], "bet": 0}
                    elif current_user:
                        if line.startswith("Player Cards:"):
                            game_data[current_user]["player_cards"] = line.split("Player Cards:")[1].strip().split(", ")
                        elif line.startswith("Dealer Cards:"):
                            game_data[current_user]["dealer_cards"] = line.split("Dealer Cards:")[1].strip().split(", ")
                        elif line.startswith("Bet:"):
                            game_data[current_user]["bet"] = int(line.split("Bet:")[1].strip())
        except FileNotFoundError:
            raise FileNotFoundError("There is no saved games for this player")

        if username not in game_data:
            raise FileNotFoundError("There is no saved games for this player")

        # Create a game
        game = cls(ui)
        user_data = game_data[username]

        # Add players to the game
        dealer = Player("Croupier")
        dealer.cards = [Card(card.split(" of ")[1], card.split(" of ")[0]) for card in user_data["dealer_cards"]]
        dealer.points = dealer.sum_of_points()
        game.players.append(dealer)

        player = Player(username)
        player.cards = [Card(card.split(" of ")[1], card.split(" of ")[0]) for card in user_data["player_cards"]]
        player.current_bet = user_data["bet"]
        player.points = player.sum_of_points()
        game.players.append(player)

        return game

    def delete_game(self, username):
        '''
        This  method deletes a game, if a player returned and completed the last game
        :param username: str
        :return: None
        '''
        game_data = {}

        with open(self.saved_games, 'r') as file:
            lines = file.readlines()
            current_user = None
            for line in lines:
                line = line.strip()
                if line.startswith("User:"):
                    current_user = line.split("User:")[1].strip()
                    game_data[current_user] = {"player_cards": [], "dealer_cards": [], "bet": 0}
                elif current_user:
                    if line.startswith("Player Cards:"):
                        game_data[current_user]["player_cards"] = line.split("Player Cards:")[1].strip().split(", ")
                    elif line.startswith("Dealer Cards:"):
                        game_data[current_user]["dealer_cards"] = line.split("Dealer Cards:")[1].strip().split(", ")
                    elif line.startswith("Bet:"):
                        game_data[current_user]["bet"] = int(line.split("Bet:")[1].strip())

        if username in game_data:
            del game_data[username]

        with open(self.saved_games, 'w') as file:
            for user, data in game_data.items():
                file.write(f"User: {user}\n")
                file.write(f"Player Cards: {', '.join(data['player_cards'])}\n")
                file.write(f"Dealer Cards: {', '.join(data['dealer_cards'])}\n")
                file.write(f"Bet: {data['bet']}\n")

    def finish_game(self):
        '''
        Clean-up code after a game session ends.
        '''
        self.db.close_db()

    def win_condition(self):
        '''
        Verifies if a gamer wins a game
        :return: boolean
        '''

        sum1 = self.players[0].points  # Dealer's points
        sum2 = self.players[1].points  # Player's points
        print("You cards:", self.players[1].show_cards())
        print("You have: ", self.players[1].points)
        print("Croupier cards :", self.players[0].show_cards())
        print("Casino has: ", self.players[0].points)

        player_bet = self.players[1].current_bet
        player_score = self.players[1].score
        player_name = self.players[1].get_name()

        # Flips all dealer cards that are covered
        for i in range(1,len(self.ui.dealer.cards)):
            self.ui.dealer.flip_card(self.ui.dealer.get_cards()[i],0)
        # If the player surrendered or exceeded 21 points, they lose
        if sum2 > 21:
            print(f"You got more than 21!")
            self.ui.if_lost()
            return False
        # If the player has exactly 21 points and the dealer does not, the player wins
        if sum2 == 21 and sum1 != 21:
            player_score += player_bet * 2
            self.players[1].db.update_highscore(player_name, player_score)
            self.ui.if_won()
            print(f"You win! You will get your bet doubled!")
            return True
        # If the dealer's points exceed 21, the player wins
        if sum1 > 21:
            player_score += player_bet  # return player's bet
            self.players[1].db.update_highscore(player_name, player_score)
            self.ui.if_won()
            print(f"Croupier has more than 21. You will get your bet back!")
            return True
        # If both the player and the dealer have exactly 21 points or the same points, it's a push, and the player gets their bet back
        if sum1 == sum2:
            player_score += player_bet  # return bet
            self.players[1].db.update_highscore(player_name, player_score)
            self.ui.if_won()
            print(f"It's push! You will get your bet back!")
            return True
        # If the player's points are greater than the dealer's, the player wins
        if sum2 > sum1:
            player_score += player_bet * 1.5
            self.players[1].db.update_highscore(player_name, player_score)
            print(f"You win! You will get 1.5 of your bet!")
            self.ui.if_won()
            return True
        # If the player's points are lesser than the dealer's, the player looses
        if sum2 < sum1:
            self.ui.if_lost()
            return False

    def dealers_turn(self):
        dealer = Player("Croupier")
        self.add_player(dealer)
        self.ui.setup_dealer()
        print("Now Croupier takes the first card")
        card = self.deck.deal()
        dealer.add_card(card)
        self.ui.dealer.add_card(card,False)
        print(f"{dealer.name} draws:", dealer.show_cards())  # the fist card of Croupier
        while dealer.points < 17:  # dealer takes cards as long as he does not have at least 17
            card = self.deck.deal()
            dealer.add_card(card)
            self.ui.dealer.add_card(card, True)

    def players_turn(self, player: Player):
        self.add_player(player)
        print("Now it's your turn")
        card = self.deck.deal()
        player.add_card(card)
        self.ui.currentPlayer.add_card(card,self.ui.count_hand)
        card = self.deck.deal()
        player.add_card(card)
        self.ui.currentPlayer.add_card(card,self.ui.count_hand)
        print("You cards:", player.show_cards())
        """
        print("You have", player.score, "scores")
        print("How many scores do you want to bet?")
        
        while True:
            bet = int(input())
            if player.make_bet(bet):
                break
            else:
                print("How many scores do you want to bet?")
                continue
    """
    def run(self, player: Player):
        '''
        Runs a game session interactively until a win (or lose) condition is reached.
        '''
        self.dealers_turn()
        self.players_turn(player)
        print("Do you want to save a game and leave?")
        answer = str(input())
        if answer == 'Yes':
            self.save_game(player.get_name())
            self.finish_game()
            return -1
        if player.double_down():
            card = self.deck.deal()
            player.add_card(card)
            self.win_condition()
            self.finish_game()
            return 0
        self.hit()
        if self.win_condition():
            self.finish_game()
        else:
            print(f"You lose!")
            self.finish_game()

    def run_if_loaded(self):
        player1 = self.players[0]  # Croupier
        player2 = self.players[1]
        print("Your cards:", player2.show_cards())
        print("Your bet:", player2.current_bet)
        if player2.double_down():
            player2.add_card(self.deck.deal())
            if self.win_condition():
                self.delete_game(player2.get_name())
                self.finish_game()
                return 0
            else:
                print(f"You lose!")
                self.delete_game(player2.get_name())
                self.finish_game()
                return 0
        self.hit()
        if self.win_condition():
            self.delete_game(player2.get_name())
            self.finish_game()
        else:
            print(f"You lose!")
            self.delete_game(player2.get_name())
            self.finish_game()
